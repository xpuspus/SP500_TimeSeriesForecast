# SP500_TimeSeriesForecast
This repository includes the notebook and all other necessary resources in order to perform time-series analysis and forecast on the S&P500 data.

Please download the [data](https://drive.google.com/drive/folders/1xyw9JAJU0vkhp0Lq0bnNEjdkSLqw_Q4z) in the same folder as the notebook and run the notebook.
